package com.puntodamar.limitservice.controller;


import com.puntodamar.limitservice.configuration.ApplicationConfiguration;
import com.puntodamar.limitservice.bean.LimitConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LimitConfigurationController {

    @Autowired
    private ApplicationConfiguration applicationConfiguration;

    @GetMapping("/limits")
    public LimitConfiguration retrieveLimitsFromConfigurations(){
        return new LimitConfiguration(applicationConfiguration.getMinimum(), applicationConfiguration.getMaximum());
    }
}
